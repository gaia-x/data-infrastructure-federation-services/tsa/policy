# github.com/Microsoft/go-winio v0.4.16
## explicit; go 1.12
github.com/Microsoft/go-winio
github.com/Microsoft/go-winio/pkg/guid
# github.com/ProtonMail/go-crypto v0.0.0-20210428141323-04723f9f07d7
## explicit; go 1.13
github.com/ProtonMail/go-crypto/bitcurves
github.com/ProtonMail/go-crypto/brainpool
github.com/ProtonMail/go-crypto/eax
github.com/ProtonMail/go-crypto/internal/byteutil
github.com/ProtonMail/go-crypto/ocb
github.com/ProtonMail/go-crypto/openpgp
github.com/ProtonMail/go-crypto/openpgp/aes/keywrap
github.com/ProtonMail/go-crypto/openpgp/armor
github.com/ProtonMail/go-crypto/openpgp/ecdh
github.com/ProtonMail/go-crypto/openpgp/elgamal
github.com/ProtonMail/go-crypto/openpgp/errors
github.com/ProtonMail/go-crypto/openpgp/internal/algorithm
github.com/ProtonMail/go-crypto/openpgp/internal/ecc
github.com/ProtonMail/go-crypto/openpgp/internal/encoding
github.com/ProtonMail/go-crypto/openpgp/packet
github.com/ProtonMail/go-crypto/openpgp/s2k
# github.com/acomagu/bufpipe v1.0.3
## explicit; go 1.12
github.com/acomagu/bufpipe
# github.com/emirpasic/gods v1.12.0
## explicit
github.com/emirpasic/gods/containers
github.com/emirpasic/gods/lists
github.com/emirpasic/gods/lists/arraylist
github.com/emirpasic/gods/trees
github.com/emirpasic/gods/trees/binaryheap
github.com/emirpasic/gods/utils
# github.com/go-git/gcfg v1.5.0
## explicit
github.com/go-git/gcfg
github.com/go-git/gcfg/scanner
github.com/go-git/gcfg/token
github.com/go-git/gcfg/types
# github.com/go-git/go-billy/v5 v5.3.1
## explicit; go 1.13
github.com/go-git/go-billy/v5
github.com/go-git/go-billy/v5/helper/chroot
github.com/go-git/go-billy/v5/helper/polyfill
github.com/go-git/go-billy/v5/memfs
github.com/go-git/go-billy/v5/osfs
github.com/go-git/go-billy/v5/util
# github.com/go-git/go-git/v5 v5.4.2
## explicit; go 1.13
github.com/go-git/go-git/v5
github.com/go-git/go-git/v5/config
github.com/go-git/go-git/v5/internal/revision
github.com/go-git/go-git/v5/internal/url
github.com/go-git/go-git/v5/plumbing
github.com/go-git/go-git/v5/plumbing/cache
github.com/go-git/go-git/v5/plumbing/color
github.com/go-git/go-git/v5/plumbing/filemode
github.com/go-git/go-git/v5/plumbing/format/config
github.com/go-git/go-git/v5/plumbing/format/diff
github.com/go-git/go-git/v5/plumbing/format/gitignore
github.com/go-git/go-git/v5/plumbing/format/idxfile
github.com/go-git/go-git/v5/plumbing/format/index
github.com/go-git/go-git/v5/plumbing/format/objfile
github.com/go-git/go-git/v5/plumbing/format/packfile
github.com/go-git/go-git/v5/plumbing/format/pktline
github.com/go-git/go-git/v5/plumbing/object
github.com/go-git/go-git/v5/plumbing/protocol/packp
github.com/go-git/go-git/v5/plumbing/protocol/packp/capability
github.com/go-git/go-git/v5/plumbing/protocol/packp/sideband
github.com/go-git/go-git/v5/plumbing/revlist
github.com/go-git/go-git/v5/plumbing/storer
github.com/go-git/go-git/v5/plumbing/transport
github.com/go-git/go-git/v5/plumbing/transport/client
github.com/go-git/go-git/v5/plumbing/transport/file
github.com/go-git/go-git/v5/plumbing/transport/git
github.com/go-git/go-git/v5/plumbing/transport/http
github.com/go-git/go-git/v5/plumbing/transport/internal/common
github.com/go-git/go-git/v5/plumbing/transport/server
github.com/go-git/go-git/v5/plumbing/transport/ssh
github.com/go-git/go-git/v5/storage
github.com/go-git/go-git/v5/storage/filesystem
github.com/go-git/go-git/v5/storage/filesystem/dotgit
github.com/go-git/go-git/v5/storage/memory
github.com/go-git/go-git/v5/utils/binary
github.com/go-git/go-git/v5/utils/diff
github.com/go-git/go-git/v5/utils/ioutil
github.com/go-git/go-git/v5/utils/merkletrie
github.com/go-git/go-git/v5/utils/merkletrie/filesystem
github.com/go-git/go-git/v5/utils/merkletrie/index
github.com/go-git/go-git/v5/utils/merkletrie/internal/frame
github.com/go-git/go-git/v5/utils/merkletrie/noder
# github.com/golang/snappy v0.0.1
## explicit
github.com/golang/snappy
# github.com/imdario/mergo v0.3.12
## explicit; go 1.13
github.com/imdario/mergo
# github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99
## explicit
github.com/jbenet/go-context/io
# github.com/kelseyhightower/envconfig v1.4.0
## explicit
github.com/kelseyhightower/envconfig
# github.com/kevinburke/ssh_config v0.0.0-20201106050909-4977a11b4351
## explicit
github.com/kevinburke/ssh_config
# github.com/klauspost/compress v1.13.6
## explicit; go 1.15
github.com/klauspost/compress
github.com/klauspost/compress/fse
github.com/klauspost/compress/huff0
github.com/klauspost/compress/internal/snapref
github.com/klauspost/compress/zstd
github.com/klauspost/compress/zstd/internal/xxhash
# github.com/mitchellh/go-homedir v1.1.0
## explicit
github.com/mitchellh/go-homedir
# github.com/montanaflynn/stats v0.0.0-20171201202039-1bf9dbcd8cbe
## explicit
github.com/montanaflynn/stats
# github.com/pkg/errors v0.9.1
## explicit
github.com/pkg/errors
# github.com/sergi/go-diff v1.1.0
## explicit; go 1.12
github.com/sergi/go-diff/diffmatchpatch
# github.com/xanzy/ssh-agent v0.3.0
## explicit
github.com/xanzy/ssh-agent
# github.com/xdg-go/pbkdf2 v1.0.0
## explicit; go 1.9
github.com/xdg-go/pbkdf2
# github.com/xdg-go/scram v1.1.1
## explicit; go 1.11
github.com/xdg-go/scram
# github.com/xdg-go/stringprep v1.0.3
## explicit; go 1.11
github.com/xdg-go/stringprep
# github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d
## explicit
github.com/youmark/pkcs8
# go.mongodb.org/mongo-driver v1.10.2
## explicit; go 1.10
go.mongodb.org/mongo-driver/bson
go.mongodb.org/mongo-driver/bson/bsoncodec
go.mongodb.org/mongo-driver/bson/bsonoptions
go.mongodb.org/mongo-driver/bson/bsonrw
go.mongodb.org/mongo-driver/bson/bsontype
go.mongodb.org/mongo-driver/bson/primitive
go.mongodb.org/mongo-driver/event
go.mongodb.org/mongo-driver/internal
go.mongodb.org/mongo-driver/internal/randutil
go.mongodb.org/mongo-driver/internal/randutil/rand
go.mongodb.org/mongo-driver/internal/uuid
go.mongodb.org/mongo-driver/mongo
go.mongodb.org/mongo-driver/mongo/address
go.mongodb.org/mongo-driver/mongo/description
go.mongodb.org/mongo-driver/mongo/options
go.mongodb.org/mongo-driver/mongo/readconcern
go.mongodb.org/mongo-driver/mongo/readpref
go.mongodb.org/mongo-driver/mongo/writeconcern
go.mongodb.org/mongo-driver/tag
go.mongodb.org/mongo-driver/version
go.mongodb.org/mongo-driver/x/bsonx
go.mongodb.org/mongo-driver/x/bsonx/bsoncore
go.mongodb.org/mongo-driver/x/mongo/driver
go.mongodb.org/mongo-driver/x/mongo/driver/auth
go.mongodb.org/mongo-driver/x/mongo/driver/auth/internal/awsv4
go.mongodb.org/mongo-driver/x/mongo/driver/auth/internal/gssapi
go.mongodb.org/mongo-driver/x/mongo/driver/connstring
go.mongodb.org/mongo-driver/x/mongo/driver/dns
go.mongodb.org/mongo-driver/x/mongo/driver/mongocrypt
go.mongodb.org/mongo-driver/x/mongo/driver/mongocrypt/options
go.mongodb.org/mongo-driver/x/mongo/driver/ocsp
go.mongodb.org/mongo-driver/x/mongo/driver/operation
go.mongodb.org/mongo-driver/x/mongo/driver/session
go.mongodb.org/mongo-driver/x/mongo/driver/topology
go.mongodb.org/mongo-driver/x/mongo/driver/wiremessage
# golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d
## explicit; go 1.17
golang.org/x/crypto/blowfish
golang.org/x/crypto/cast5
golang.org/x/crypto/chacha20
golang.org/x/crypto/curve25519
golang.org/x/crypto/curve25519/internal/field
golang.org/x/crypto/ed25519
golang.org/x/crypto/internal/poly1305
golang.org/x/crypto/internal/subtle
golang.org/x/crypto/ocsp
golang.org/x/crypto/pbkdf2
golang.org/x/crypto/ssh
golang.org/x/crypto/ssh/agent
golang.org/x/crypto/ssh/internal/bcrypt_pbkdf
golang.org/x/crypto/ssh/knownhosts
# golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2
## explicit; go 1.17
golang.org/x/net/context
golang.org/x/net/internal/socks
golang.org/x/net/proxy
# golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
## explicit
golang.org/x/sync/errgroup
# golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1
## explicit; go 1.17
golang.org/x/sys/cpu
golang.org/x/sys/execabs
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.3.7
## explicit; go 1.17
golang.org/x/text/transform
golang.org/x/text/unicode/norm
# gopkg.in/warnings.v0 v0.1.2
## explicit
gopkg.in/warnings.v0
