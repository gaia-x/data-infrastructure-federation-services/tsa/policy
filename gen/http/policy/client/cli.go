// Code generated by goa v3.8.5, DO NOT EDIT.
//
// policy HTTP client CLI support package
//
// Command:
// $ goa gen
// gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/policy/design

package client

import (
	"encoding/json"
	"fmt"
	"strconv"

	policy "gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/policy/gen/policy"
)

// BuildEvaluatePayload builds the payload for the policy Evaluate endpoint
// from CLI flags.
func BuildEvaluatePayload(policyEvaluateBody string, policyEvaluateGroup string, policyEvaluatePolicyName string, policyEvaluateVersion string, policyEvaluateEvaluationID string, policyEvaluateTTL string) (*policy.EvaluateRequest, error) {
	var err error
	var body interface{}
	{
		err = json.Unmarshal([]byte(policyEvaluateBody), &body)
		if err != nil {
			return nil, fmt.Errorf("invalid JSON for body, \nerror: %s, \nexample of valid JSON:\n%s", err, "\"Ipsum nihil quo.\"")
		}
	}
	var group string
	{
		group = policyEvaluateGroup
	}
	var policyName string
	{
		policyName = policyEvaluatePolicyName
	}
	var version string
	{
		version = policyEvaluateVersion
	}
	var evaluationID *string
	{
		if policyEvaluateEvaluationID != "" {
			evaluationID = &policyEvaluateEvaluationID
		}
	}
	var ttl *int
	{
		if policyEvaluateTTL != "" {
			var v int64
			v, err = strconv.ParseInt(policyEvaluateTTL, 10, strconv.IntSize)
			val := int(v)
			ttl = &val
			if err != nil {
				return nil, fmt.Errorf("invalid value for ttl, must be INT")
			}
		}
	}
	v := body
	res := &policy.EvaluateRequest{
		Input: &v,
	}
	res.Group = group
	res.PolicyName = policyName
	res.Version = version
	res.EvaluationID = evaluationID
	res.TTL = ttl

	return res, nil
}

// BuildLockPayload builds the payload for the policy Lock endpoint from CLI
// flags.
func BuildLockPayload(policyLockGroup string, policyLockPolicyName string, policyLockVersion string) (*policy.LockRequest, error) {
	var group string
	{
		group = policyLockGroup
	}
	var policyName string
	{
		policyName = policyLockPolicyName
	}
	var version string
	{
		version = policyLockVersion
	}
	v := &policy.LockRequest{}
	v.Group = group
	v.PolicyName = policyName
	v.Version = version

	return v, nil
}

// BuildUnlockPayload builds the payload for the policy Unlock endpoint from
// CLI flags.
func BuildUnlockPayload(policyUnlockGroup string, policyUnlockPolicyName string, policyUnlockVersion string) (*policy.UnlockRequest, error) {
	var group string
	{
		group = policyUnlockGroup
	}
	var policyName string
	{
		policyName = policyUnlockPolicyName
	}
	var version string
	{
		version = policyUnlockVersion
	}
	v := &policy.UnlockRequest{}
	v.Group = group
	v.PolicyName = policyName
	v.Version = version

	return v, nil
}
